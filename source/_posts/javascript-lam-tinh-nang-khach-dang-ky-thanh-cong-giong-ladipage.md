---
title: (JavaScript) Làm tính năng " Khách Đăng ký thành công" giống Ladipage
date: Jan 8, 2022
updated: Jan 19, 2023
categories: tips
description: HTML CSS JS tạo chức năng đăng ký thành công fake giống ladipage, có video hướng dẫn
thumbnail: /images/posts/javascript-dang-ky-giong-ladipage.jpeg
sticky: 1
excerpt: HTML CSS JS tạo chức năng đăng ký thành công fake giống ladipage
---
## Hãy xem video nếu cần hiểu 
{% youtube rZDkXGxWuRE %}

## Copy code về sửa chơi này
{% codeblock HTML/CSS/JS lang:html https://maivietson.com Nguồn: Mai Việt Sơn%}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notify</title>
  </head>
  <body>
    <div id="mvs__notify">
      <h3 id="mvs__title">Mai Viet Son</h3>
      <p>Vừa đăng ký thành công khóa học JS cơ bản</p>
    </div>
    <style>
      #mvs__notify {
        position: fixed;
        bottom: 20px;
        left: 20px;
        line-height: 10px;
        background: #002d5f;
        color: white;
        padding: 5px20px;
        border-radius: 20px;
        transition: 2sease;
        opacity: 0;
      }

      .mvs__show {
        opacity: 1 !important;
      }
    </style>
    <script>
      varmvsNotify = {
        dataCustomer: [
          'Mai Việt Sơn', 
          'Nguyễn Tuấn Anh', 
          'Lương Xuân Trường', 
          'Quế Ngọc Hải', 
          'Park Hang Seo', 
          'Nguyễn Hữu Thắng', 
          'Nguyễn Quang Hải'
          ],
        dataTitle: 'Mai Việt Sơn',
        mvsBox: document.getElementById('mvs__notify'),
        mvsCus: document.getElementById('mvs__title'),
        init: function() {
          varnewShow = this.show.bind(this);
          varnewHide = this.hide.bind(this);
          varTimer = setInterval(function() {
            newShow();
            setTimeout(function() {
              newHide();
            }, 3000);
          }, 8000);
        },
        show: function() {
          this.mvsCus.innerText = this.dataCustomer[this.getIndex()];
          this.mvsBox.classList.add('mvs__show');
        },
        hide: function() {
          this.mvsBox.classList.remove('mvs__show');
        },
        getIndex: function() {
          returnMath.floor(Math.random() * this.dataCustomer.length);
        }
      }
      mvsNotify.init();
    </script>
  </body>
</html>
{%endcodeblock%}