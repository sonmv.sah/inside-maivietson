---
title: (Docs) Tổng hợp kinh nghiệm khi dùng Tailwindcss trên Safari
categories:
  - docs

thumbnail: /images/docs/fix-loi-503-nginx-bad-gateway.jpeg
excerpt: Tổng hợp kinh nghiệm khi dùng Tailwindcss trên Safari với dự án thực tế 
description: Tổng hợp kinh nghiệm khi dùng Tailwindcss trên Safari
sticky: 1
---

Đây là bản lưu trữ lại kinh nghiệm khi làm việc với Tailwindcss mà trong thực tế mình thực hiện các dự án có gặp phải.

## Tailwindcss là gì

Tailwindcss là 1 css framework giúp tạo giao diện website và tuỳ biến giao diện website thông qua các bộ class thay thế file css thường.

## Chạy mượt với nhân chromium

Tailwindcss rất tối ưu cho các trình duyệt chạy nhân Chromium nên các trình duyệt như Chrome, brave, Cốc cốc,... được tối ưu tốt và dường như ko có lỗi vặt.

## Tailwindcss đôi lúc sẽ bị "hóc xương" Safari 

Thực sự thì safari có lẽ là gì đó anh em không thích rồi tuy nhiên nó vẫn là một trình duyệt được đông đảo anh em sử dụng nhất là "Ifan" lại không phải ít.

Trong thực tế khi sử dụng Safari đôi khi css không hiểu là bị xung đột hay chưa được tối ưu tốt nên nó không nhận css mặc dù bên chrome chạy ngon.

## Tổng hợp các kinh nghiệm xử lý

::: timeline
- [1] Lỗi không chạy `gap-` khi dùng flex có justify
    Ví dụ: Sử dụng `flex justify-start gap-10` thì ngay lập tức `gap-10` sẽ không nhận css và lỗi ở đoạn này. 
    ### Cách xử lý
    Hãy sử dụng `space-x-10` và bỏ 'justify-start' đi nhé chúng ta có:
   ```bash
   class="flex space-x-10"
   ```
- [2] Lỗi không chạy `text-nowrap` 
    Ví dụ: Sử dụng `text-nowrap` cho các phần tử muốn để `max-width: max-content` chúng ta không đạt được kết quả. 
    ### Cách xử lý
    Hãy sử dụng thêm `white-space-pre` và `break-keep` chúng ta có:
   ```bash
   class="white-space-pre break-keep"
   ```
  - [3] updating...
:::