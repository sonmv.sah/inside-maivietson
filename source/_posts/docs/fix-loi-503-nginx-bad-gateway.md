---
title: (Docs) Sửa lỗi 502 bad gateway nginx, do buffering/timeout chưa tốt
categories:
  - docs

thumbnail: /images/docs/fix-loi-503-nginx-bad-gateway.jpeg
excerpt: Hướng dẫn sửa lỗi 502 Bad gateway cho nginx sử dụng cho Centos thành công với dự án thực tế đa ngữ
description: Hướng dẫn sửa lỗi 502 Bad gateway cho nginx sử dụng cho Centos thành công với dự án thực tế đa ngữ
sticky: 1
---
Khoảng năm 2022 mình có thực hiện 1 sự án đa ngữ với wordpress/nginx server và khi mình chuyển request $_GET qua lại giữa các ngôn ngữ thì bị lỗi *502 bad gate way *

Và mình đã dùng cách sau để xử lý , mình lưu lại với tiền tố **[docs]** chính là loạt bài thư viện code mình từng xử lý qua để sau khi gặp các vấn đề dễ dàng tìm lại cũng như chia sẻ với các bạn chưa biết

## Tình trạng và công nghệ

- [x] Wordpress cms
- [x] Nginx Server/ Centos 7 os / Vps azdigi
- [x] Hocvps Script

## Cách khắc phục

Mình không đi vào các vấn đề lý do mà sẽ đưa ra các bước thực hiện, các bạn có thể tìm hiểu thêm về lý do sau nhé

::: timeline
- [1] Tắt nginx service:
  ```bash
    service nginx stop
  ```
- [2] Mở file config của nginx cho domain tương ứng
  ```bash
    nano /etc/nginx/conf.d/domain.com.conf
  ```
- [3] Tìm đoạn code sau trong block http{...}:
  ```bash
    fastcgi_buffers 8 16k;
    fastcgi_buffer_size 16k;
  ```
    Thay thành đoạn sau:
  ```bash
    fastcgi_buffers 64 64k;
    fastcgi_buffer_size 64k;
  ```
- [4] Khởi động lại nginx service:
  ```bash
    service nginx restart
  ```
:::