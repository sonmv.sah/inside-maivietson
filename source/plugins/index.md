---
title: Plugins by Mai Việt Sơn
thumbnail: https://www.vib.com.vn/wps/wcm/connect/a46008f3-42d7-4f0b-888a-03c72c208c77/LOGO-VIB.png?MOD=AJPERES
excerpt: Danh sách Plugin của tác giả Mai Việt Sơn, Bao gồm các plugin miễn phí và có trả phí
---
## Danh sách Plugin trả phí ?

::: timeline
 - [ ] [Plugin random phong thuỷ](#)
:::
<!-- more -->
1. Trang web mà bạn vừa truy cập đang hết hạn sử dụng và cần được gia hạn

2. Trang website đã sử dụng trộm giao diện của chúng tôi mà không thông qua sự cho phép hoặc mua từ chính chủ!

## Danh sách Plugin miễn phí
1. Nếu bạn là chủ website vui và muốn tiếp tục sử dụng website được tích hợp sản phẩm của tôi ( Mai Việt Sơn) thì bạn vui lòng liên hệ thông tin bên dưới

2. Nếu bạn là khách hàng thì rất tiếc vì hiện tại website cần được gia hạn mới có thể sử dụng tiếp! Chân thành cảm ơn!

## Thông tin của tôi
```bash
Mai Việt Sơn
Điện thoại/ Zalo: 0936952588
Email: sonmv.ivcu@gmail.com
```
## Thông tin thanh toán


![image](https://www.vib.com.vn/wps/wcm/connect/a46008f3-42d7-4f0b-888a-03c72c208c77/LOGO-VIB.png?MOD=AJPERES)


```bash
Chủ tài khoản : Mai Việt Sơn
Số tài khoản: 0247 0406 0258 914
Chi nhánh: Ngân hàng Quốc Tế (VIB), chi nhánh Nguyễn Lương Bằng, TP. Hải Dương
```
