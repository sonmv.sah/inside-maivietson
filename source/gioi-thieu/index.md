---
title: Giới thiệu Mai Việt Sơn Blog | Dịch vụ thiết kế website
description: Blog Kiến thức thiết kế website Mai Việt Sơn, Mang tới một góc nhỏ các kỹ năng, kinh nghiệm trong thiết kế website, và cũng mang tới dịch vụ thiết kế website
thumbnail: /images/logo.jpeg
photos:
    - /images/logo.jpeg
color: yellow
---

## Lời tựa!

Mình cũng từ người không biết gì và có thể gọi là tần tảo trong cái món nghề này, tự tìm, tự đọc rồi cũng thấy nhiều tại liệu đọc mãi không vào, Thật may là có những Blog nổi tiếng như của anh ***Thạch Phạm***,… Nhiều lắm, chắc em xin không nhắc tên.

Đương nhiên mình cũng ***biết mình ở vị trí nào***. Mình chỉ có một suy rằng, vậy đấy, dù sao thì mỗi người có một cái duyên, có một **tần số để bắt sóng**.

Nói dễ hiểu hơn thì, Cùng một vấn đề anh A nói bạn không hiểu được nhưng đọc bài của anh B thì chúng ta lại có thể dễ hiểu hơn rất nhiều. Mình không nói rằng anh A ko giỏi mà ý rằng cái duyên của mình chỉ có với anh B .

Vậy nên Blog này cũng mong mang cái duyên lành ấy đến những người có thể đọc và hiểu được ngữ điệu của mình.

Đương nhiên mình cũng luôn cải thiện cách viết để giúp nhiều người hơn nữa đọc hiểu các vấn đề.

Có điều này mình muốn nói.

{% blockquote Chốt Vấn Đề %}
***Chỉ có biết trước và biết sau, không có giỏi hơn đâu, các bạn chỉ cần đọc và thực hành nhé! Kèm thêm chút duyên nữa là có thể làm tốt các việc thôi*** 
{% endblockquote %}

Thực sự tôi đã mất tới **2 năm** từ khi xây dựng giao diện Blog này cho tới khi viết bài viết đầu tiên này.

Lý do đơn giản là kiến thức của tôi chỉ là phần thật nhỏ bé, có câu ***“Thiên ngoại hữu thiên”*** mà !

Tuy nhiên tôi lại tâm đắc với suy nghĩ: **Có Duyên ắt có gặp**

Có thể nhiều các nhân vật có kiến thức sâu rộng ở ngoài kia nhưng **chúng ta lại là người có duyên** với nhau vậy nên ***bạn lại là người hiểu được tôi viết gì*** !

![image](/images/logo.jpeg)